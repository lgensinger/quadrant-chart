# Quadrant Chart

ES6 d3.js x/y quadrant chart visualization.

## Install

```bash
# install package
npm install @lgv/quadrant-chart
```

## Data Format

The following values are the expected input data structure; id is optional.

```json
[
    {
        "id": 1,
        "label": "this is a title",
        "description": "this is a description",
        "x": 3,
        "y": 5
    },
    {
        "id": 2,
        "label": "this is a title",
        "description": "this is a description",
        "x": 1,
        "y": 2
    }
]
```

## Use Module

```bash
import { QuadrantChart } from "@lgv/quadrant-chart";

// have some data
let data = [
    {
        "id": 1,
        "label": "this is a title",
        "description": "this is a description",
        "x": 3,
        "y": 5
    },
    {
        "id": 2,
        "label": "this is a title",
        "description": "this is a description",
        "x": 1,
        "y": 2
    }
];

// initialize
const qc = new QuadrantChart(data);

// render visualization
qc.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_WIDTH` | integer | width of artboard |

## Events

The following events are dispatched from the svg element. Hover events toggle a class named `active` on the element.

| Target | Name | Event |
| :-- | :-- | :-- |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-quadrant-chart` | top-level svg element |
| `lgv-content` | content inside artboard inside padding |

## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd quadrant-chart

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run startdocker

# edit src/index.js
# add const qc = new QuadrantChart(data);
# add qc.render(document.body);
# replace `data` with whatever data you want to develop with

# view visualization in browser at http://localhost:8080
```
