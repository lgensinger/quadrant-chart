import test from "ava";

import { configurationLayout } from "@lgv/visualization-chart";
import { configuration } from "../src/configuration.js";
import { QuadrantChart } from "../src/index.js";

/******************** EMPTY VARIABLES ********************/

// initialize
//let scc = new StackedColumnChart();

// TEST INIT //
/*test("init", t => {

    t.true(scc.height === configurationLayout.height);
    t.true(scc.width === configurationLayout.width);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    sbc.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == configurationLayout.height);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == configurationLayout.width);

});*/

/******************** DECLARED PARAMS ********************/

let testWidth = 300;
let testHeight = 300;
let testData = [{
    "id": 1,
    "label": "this is a title",
    "description": "this is a description",
    "x": 3,
    "y": 5
},
{
    "id": 2,
    "label": "this is a title",
    "description": "this is a description",
    "x": 1,
    "y": 2
}];

// initialize
let qc = new QuadrantChart(testData, testWidth, testHeight);

// TEST INIT //
test("init_params", t => {

    t.true(qc.height === testHeight);
    t.true(qc.width === testWidth);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render_params", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    sbcp.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == testHeight);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == testWidth);

});*/
