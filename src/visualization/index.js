import { QuadrantLabel, QuadrantLayout, configurationLayout, LinearGrid } from "@lgv/visualization-chart";
import { axisBottom, axisLeft } from "d3-axis";
import { scaleLinear } from "d3-scale";
import { select } from "d3-selection";
import { transition } from "d3-transition";

import { configuration } from "../configuration.js";

/**
 * QuadrantChart is an x/y quadrant visualization.
 * @param {array} data - objects where each represents a series in the collection
 * @param {integer} height - artboard height
 * @param {float} nodeSize - size of circle point
 * @param {integer} width - artboard width
 * @param {array} x - min, max, label, tick count for x axis
 * @param {array} y - min, max, label, tick count for y axis
 */
class QuadrantChart extends LinearGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, x=[null, null, "x", null], y=[null, null, "y", null], nodeSize=null, name=configuration.name, label=configuration.branding) {

        // initialize inheritance
        super(data, width, height, label, name);

        // update self
        this.axisX = null;
        this.axisY = null;
        this.node = null;
        this.classGrid = `${label}-grid`;
        this.classNode = `${label}-node`;
        this.classQuadrant = `${label}-quadrant`;
        this.classQuadrantLabel = `${label}-quadrant-label`;
        this.classXaxis = `${label}-x-axis`;
        this.classYaxis = `${label}-y-axis`;
        this.Data = new QuadrantLayout(data, x, y);
        this.grid = null;
        this.nodeSize = nodeSize || this.unit / 4;
        this.quadrant = null;
        this.x = x;
        this.y = y;

    }

    /**
     * Construct quadrants dataset.
     * @returns An array of objects where each represent a quadrant.
     */
    get quadrants() {
        return [
            {
                x: 0,
                y: 0,
                label: `low ${this.Data.x[2]} / high ${this.Data.y[2]}`
            },
            {
                x: this.width / 2,
                y: 0,
                label: `high ${this.Data.x[2]} / high ${this.Data.y[2]}`
            },
            {
                x: this.width / 2,
                y: this.height / 2,
                label: `high ${this.Data.x[2]} / low ${this.Data.y[2]}`
            },
            {
                x: 0,
                y: this.height / 2,
                label: `low ${this.Data.x[2]} / low ${this.Data.y[2]}`
            }
        ];
    }

    /**
     * Construct x axis.
     * @returns A d3 axis function.
     */
    get xAxis() {
        return axisBottom(this.xScale)
            .ticks(this.Data.xTicks)
            .tickSizeInner(this.unit / 2);
    }

    /**
     * Determine x domain difference.
     * @returns A float.
     */
    get xDomain() {
        return this.xScale.domain()[1] - this.xScale.domain()[0];
    }

    /**
     * Construct x scale.
     * @returns A d3 scale function.
     */
    get xScale() {
        return scaleLinear()
            .domain([this.Data.xMin, this.Data.xMax])
            .range([this.unit, this.width - this.unit]);
    }

    /**
     * Construct y axis.
     * @returns A d3 axis function.
     */
    get yAxis() {
        return axisLeft(this.yScale)
            .ticks(this.Data.yTicks)
            .tickSizeInner(this.unit / 2);
    }

    /**
     * Determine y domain difference.
     * @returns A float.
     */
    get yDomain() {
        return this.yScale.domain()[1] - this.yScale.domain()[0];
    }

    /**
     * Construct y scale.
     * @returns A d3 scale function.
     */
    get yScale() {
        return scaleLinear()
            .domain([this.Data.yMin, this.Data.yMax])
            .range([this.height - (this.unit * 2), this.unit * 2]);
    }

    /**
     * Declare node mouse events.
     */
    configureNodeEvents() {
        this.node
            .on("click", (e,d) => {

                // determine quadrantLabel
                let q = this.Data.extractQuadrant(d, this.xDomain, this.yDomain);

                return this.configureEvent("node-click",d,e,q);

            })
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classNode} active`);

                // determine quadrant label
                let q = this.Data.extractQuadrant(d, this.xDomain, this.yDomain);

                // send event to parent
                this.configureEvent("node-mouseover",d,e,q);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classNode);

                // send event to parent
                this.artboard.dispatch("node-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style circle nodes in SVG dom element.
     */
    configureNodes() {
        this.node
            .transition().duration(1000)
            .attr("class", this.classNode)
            .attr("data-id", d => this.Data.extractId(d))
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("data-quadrant", d => this.Data.extractQuadrant(d, this.xDomain, this.yDomain).replaceAll(" ", "-").replace("/-", ""))
            .attr("r", this.nodeSize)
            .attr("cx", d => this.xScale(d.x))
            .attr("cy", d => this.yScale(d.y))
            .attr("fill", "lightgrey");
    }

    /**
     * Position and minimally style text for quadrants in SVG dom element.
     */
    configureQuadrantLabels() {
        this.quadrantLabel
            .attr("class", this.classQuadrantLabel)
            .attr("x", d => d.x < this.width / 2 ? d.x : this.width)
            .attr("y", d => d.y < this.height / 2 ? d.y + this.Label.determineHeight(d.label) : this.height - (this.Label.determineHeight(d.label) * 0.33))
            .attr("text-anchor", d => d.x < this.width / 2 ? "start" : "end")
            .text(d => d.label);
    }

    /**
     * Position and minimally style rectangle quadrants in SVG dom element.
     */
    configureQuadrants() {
        this.quadrant
            .attr("class", this.classQuadrant)
            .attr("x", d => d.x)
            .attr("y", d => d.y)
            .attr("width", this.width / 2)
            .attr("height", this.height / 2)
            .attr("fill", "transparent");
    }

    /**
     * Position and minimally style x axis line in SVG dom element.
     */
    configureXaxis() {
        this.axisX
            .transition().duration(1000)
            .attr("class", this.classXaxis)
            .attr("transform", `translate(0,${(this.yScale.range()[0] / 2) + (this.yScale.range()[1] / 2)})`)
            .call(this.xAxis)
            .attr("stroke", "black");
    }

    /**
     * Position and minimally style y axis line in SVG dom element.
     */
    configureYaxis() {
        this.axisY
            .transition().duration(1000)
            .attr("class", this.classYaxis)
            .attr("transform", `translate(${(this.xScale.range()[1] / 2) + (this.xScale.range()[0] / 2)},0)`)
            .call(this.yAxis)
            .attr("stroke", "black");
    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // update Data
        this.Data.x = this.x;
        this.Data.y = this.y;

        // initialize labeling
        this.Label = new QuadrantLabel(this.artboard, this.Data);

        // generate nodes
        this.node = this.generateNodes(this.content);
        this.configureNodes();
        this.configureNodeEvents();

        // generate axes
        this.axisX = this.generateXaxis(this.content);
        this.configureXaxis();
        this.axisY = this.generateYaxis(this.content);
        this.configureYaxis();

        // generate quadrant labels
        this.quadrantLabel = this.generateQuadrantLabels(this.artboard);
        this.configureQuadrantLabels();

    }

    /**
     * Generate artboard and container for visualization.
     */
    generateCore() {

        // generate svg artboard
        this.artboard = this.generateArtboard(this.container);
        this.configureArtboard();

        // generate quadrants
        this.quadrant = this.generateQuadrants(this.artboard)
        this.configureQuadrants();

        // wrap for content to ensure render within artboard
        this.content = this.generateContainer(this.artboard);
        this.configureContainer();

    }

    /**
     * Construct circle selection in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateNodes(domNode) {
        return domNode
            .selectAll(`.${this.classNode}`)
            .data(this.data)
            .join(
                enter => enter.append("circle"),
                update => update,
                exit => exit.transition()
                    .duration(1000)
                    .attr("cx", 0)
                    .attr("cy", 0)
                    .attr("transform", "scale(0)")
                    .remove()
            );
    }

    /**
     * Construct text selection in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateQuadrantLabels(domNode) {
        return domNode
            .selectAll(`.${this.classQuadrantLabel}`)
            .data(this.quadrants)
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct rectangle selection in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateQuadrants(domNode) {
        return domNode
            .selectAll(`.${this.classQuadrant}`)
            .data(this.quadrants)
            .join(
                enter => enter.append("rect"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct x axis.
     * @param {node} domNode - d3.js selection
     * @returns A d3.js selection.
     */
    generateXaxis(domNode) {
        return domNode
            .selectAll(`.${this.classXaxis}`)
            .data(d => [d])
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct y axis.
     * @param {node} domNode - d3.js selection
     * @returns A d3.js selection.
     */
    generateYaxis(domNode) {
        return domNode
            .selectAll(`.${this.classYaxis}`)
            .data(d => [d])
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

};

export { QuadrantChart };
export default QuadrantChart;
