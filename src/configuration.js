import { configuration as config } from "@lgv/visualization-chart";
import packagejson from "../package.json";

const configuration = {
    branding: config.branding,
    name: packagejson.name.replace("/", "-").slice(1)
};

export { configuration };
export default configuration;
